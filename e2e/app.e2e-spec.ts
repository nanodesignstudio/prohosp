import { Prohosp2Page } from './app.po';

describe('Prohosp2 App', () => {
    let page: Prohosp2Page;

    beforeEach(() => {
        page = new Prohosp2Page();
    });

    it('should display welcome message', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('Welcome to app!');
    });
});
