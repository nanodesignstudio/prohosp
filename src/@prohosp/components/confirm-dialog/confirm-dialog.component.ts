import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector   : 'webinar-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls  : ['./confirm-dialog.component.scss']
})
export class ProhospConfirmDialogComponent
{
    public confirmMessage: string;

    constructor(public dialogRef: MatDialogRef<ProhospConfirmDialogComponent>)
    {
    }

}
