import { NgModule } from '@angular/core';
import { MatButtonModule, MatDialogModule } from '@angular/material';

import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';

@NgModule({
    declarations: [
        ProhospConfirmDialogComponent
    ],
    imports: [
        MatDialogModule,
        MatButtonModule
    ],
    entryComponents: [
        ProhospConfirmDialogComponent
    ],
})
export class ProhospConfirmDialogModule
{
}
