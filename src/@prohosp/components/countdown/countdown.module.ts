import { NgModule } from '@angular/core';

import { ProhospCountdownComponent } from '@prohosp/components/countdown/countdown.component';

@NgModule({
    declarations: [
        ProhospCountdownComponent
    ],
    exports: [
        ProhospCountdownComponent
    ],
})
export class ProhospCountdownModule
{
}
