import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatRadioModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';

import { ProhospMaterialColorPickerModule } from '@prohosp/components/material-color-picker/material-color-picker.module';
import { ProhospThemeOptionsComponent } from '@prohosp/components/theme-options/theme-options.component';

@NgModule({
    declarations: [
        ProhospThemeOptionsComponent
    ],
    imports     : [
        CommonModule,
        FormsModule,

        FlexLayoutModule,

        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatOptionModule,
        MatRadioModule,
        MatSelectModule,
        MatSlideToggleModule,

        ProhospMaterialColorPickerModule
    ],
    exports     : [
        ProhospThemeOptionsComponent
    ]
})
export class ProhospThemeOptionsModule
{
}
