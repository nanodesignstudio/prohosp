import { NgModule } from '@angular/core';

import { ProhospHighlightComponent } from '@prohosp/components/highlight/highlight.component';

@NgModule({
    declarations: [
        ProhospHighlightComponent
    ],
    exports: [
        ProhospHighlightComponent
    ],
})
export class ProhospHighlightModule
{
}
