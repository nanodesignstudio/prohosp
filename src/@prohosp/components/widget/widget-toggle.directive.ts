import { Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[webinarWidgetToggle]'
})
export class ProhospWidgetToggleDirective
{
    constructor(public el: ElementRef)
    {
    }
}
