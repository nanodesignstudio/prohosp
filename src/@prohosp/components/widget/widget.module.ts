import { NgModule } from '@angular/core';

import { ProhospWidgetComponent } from './widget.component';
import { ProhospWidgetToggleDirective } from './widget-toggle.directive';

@NgModule({
    declarations: [
        ProhospWidgetComponent,
        ProhospWidgetToggleDirective
    ],
    exports     : [
        ProhospWidgetComponent,
        ProhospWidgetToggleDirective
    ],
})
export class ProhospWidgetModule
{
}
