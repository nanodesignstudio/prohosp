import { Component, HostBinding, HostListener, Input, OnDestroy } from '@angular/core';
import { webinarAnimations } from '../../../../animations/index';
import { ProhospConfigService } from '../../../../services/config.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'webinar-nav-horizontal-collapse',
    templateUrl: './nav-horizontal-collapse.component.html',
    styleUrls  : ['./nav-horizontal-collapse.component.scss'],
    animations : webinarAnimations
})
export class ProhospNavHorizontalCollapseComponent implements OnDestroy
{
    onConfigChanged: Subscription;
    webinarSettings: any;
    isOpen = false;

    @HostBinding('class') classes = 'nav-item nav-collapse';
    @Input() item: any;

    @HostListener('mouseenter')
    open()
    {
        this.isOpen = true;
    }

    @HostListener('mouseleave')
    close()
    {
        this.isOpen = false;
    }

    constructor(
        private webinarConfig: ProhospConfigService
    )
    {
        this.onConfigChanged =
            this.webinarConfig.onConfigChanged
                .subscribe(
                    (newSettings) => {
                        this.webinarSettings = newSettings;
                    }
                );
    }

    ngOnDestroy()
    {
        this.onConfigChanged.unsubscribe();
    }
}
