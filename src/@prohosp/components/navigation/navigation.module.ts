import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatIconModule, MatRippleModule } from '@angular/material';

import { TranslateModule } from '@ngx-translate/core';

import { ProhospNavigationComponent } from './navigation.component';
import { ProhospNavVerticalItemComponent } from './vertical/nav-item/nav-vertical-item.component';
import { ProhospNavVerticalCollapseComponent } from './vertical/nav-collapse/nav-vertical-collapse.component';
import { ProhospNavVerticalGroupComponent } from './vertical/nav-group/nav-vertical-group.component';
import { ProhospNavHorizontalItemComponent } from './horizontal/nav-item/nav-horizontal-item.component';
import { ProhospNavHorizontalCollapseComponent } from './horizontal/nav-collapse/nav-horizontal-collapse.component';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,

        MatIconModule,
        MatRippleModule,

        TranslateModule.forChild()
    ],
    exports     : [
        ProhospNavigationComponent
    ],
    declarations: [
        ProhospNavigationComponent,
        ProhospNavVerticalGroupComponent,
        ProhospNavVerticalItemComponent,
        ProhospNavVerticalCollapseComponent,
        ProhospNavHorizontalItemComponent,
        ProhospNavHorizontalCollapseComponent
    ]
})
export class ProhospNavigationModule
{
}
