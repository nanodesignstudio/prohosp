import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatButtonModule, MatIconModule, MatMenuModule, MatRippleModule } from '@angular/material';

import { ProhospPipesModule } from '@prohosp/pipes/pipes.module';

import { ProhospMaterialColorPickerComponent } from '@prohosp/components/material-color-picker/material-color-picker.component';

@NgModule({
    declarations: [
        ProhospMaterialColorPickerComponent
    ],
    imports: [
        CommonModule,

        FlexLayoutModule,

        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatRippleModule,

        ProhospPipesModule
    ],
    exports: [
        ProhospMaterialColorPickerComponent
    ],
})
export class ProhospMaterialColorPickerModule
{
}
