import { NgModule } from '@angular/core';

import { ProhospSidebarComponent } from './sidebar.component';

@NgModule({
    declarations: [
        ProhospSidebarComponent
    ],
    exports     : [
        ProhospSidebarComponent
    ]
})
export class ProhospSidebarModule
{
}
