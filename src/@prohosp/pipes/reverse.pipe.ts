import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'reverse'
})
export class ReversePipe {
    transform(arr) {
        if (!!!arr)
            return false;
        var copy = arr.slice();
        return copy.reverse();
    }
}