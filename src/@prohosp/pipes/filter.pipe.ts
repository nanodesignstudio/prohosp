import { Pipe, PipeTransform } from '@angular/core';
import { ProhospUtils } from '@prohosp/utils';

@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform
{
    transform(mainArr: any[], searchText: string, property: string): any
    {
        return ProhospUtils.filterArrayByString(mainArr, searchText);
    }
}
