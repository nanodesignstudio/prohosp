import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { WEBINAR_CONFIG, ProhospConfigService } from '@prohosp/services/config.service';
import { ProhospCopierService } from '@prohosp/services/copier.service';
import { ProhospMatchMediaService } from '@prohosp/services/match-media.service';
import { ProhospMatSidenavHelperService } from '@prohosp/directives/webinar-mat-sidenav/webinar-mat-sidenav.service';
import { ProhospNavigationService } from '@prohosp/components/navigation/navigation.service';
import { ProhospSidebarService } from '@prohosp/components/sidebar/sidebar.service';
import { ProhospSplashScreenService } from '@prohosp/services/splash-screen.service';
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';

import { ProhospFormService } from '@prohosp/services/forms';
import { ProhospCustomValidators } from '@prohosp/services/form.custom-validators';

@NgModule({
    entryComponents: [],
    providers      : [
        ProhospConfigService,
        ProhospCopierService,
        ProhospMatchMediaService,
        ProhospMatSidenavHelperService,
        ProhospNavigationService,
        ProhospSidebarService,
        ProhospSplashScreenService,
        ProhospTranslationLoaderService,
        ProhospFormService,
        ProhospCustomValidators
    ]
})
export class ProhospModule
{
    constructor(@Optional() @SkipSelf() parentModule: ProhospModule)
    {
        if ( parentModule )
        {
            throw new Error('ProhospModule is already loaded. Import it in the AppModule only!');
        }
    }

    static forRoot(config): ModuleWithProviders
    {
        return {
            ngModule : ProhospModule,
            providers: [
                {
                    provide : WEBINAR_CONFIG,
                    useValue: config
                }
            ]
        };
    }
}
