import { Directive, Input, OnInit, HostListener, OnDestroy, HostBinding } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs/Subscription';

import { ProhospMatchMediaService } from '@prohosp/services/match-media.service';
import { ProhospMatSidenavHelperService } from '@prohosp/directives/webinar-mat-sidenav/webinar-mat-sidenav.service';

@Directive({
    selector: '[webinarMatSidenavHelper]'
})
export class ProhospMatSidenavHelperDirective implements OnInit, OnDestroy
{
    matchMediaSubscription: Subscription;
    @HostBinding('class.mat-is-locked-open') isLockedOpen = true;
    @Input('webinarMatSidenavHelper') id: string;
    @Input('mat-is-locked-open') matIsLockedOpenBreakpoint: string;

    constructor(
        private webinarMatSidenavService: ProhospMatSidenavHelperService,
        private webinarMatchMedia: ProhospMatchMediaService,
        private observableMedia: ObservableMedia,
        private matSidenav: MatSidenav
    )
    {
    }

    ngOnInit()
    {
        this.webinarMatSidenavService.setSidenav(this.id, this.matSidenav);

        if ( this.observableMedia.isActive(this.matIsLockedOpenBreakpoint) )
        {
            this.isLockedOpen = true;
            this.matSidenav.mode = 'side';
            this.matSidenav.toggle(true);
        }
        else
        {
            this.isLockedOpen = false;
            this.matSidenav.mode = 'over';
            this.matSidenav.toggle(false);
        }

        this.matchMediaSubscription = this.webinarMatchMedia.onMediaChange.subscribe(() => {
            if ( this.observableMedia.isActive(this.matIsLockedOpenBreakpoint) )
            {
                this.isLockedOpen = true;
                this.matSidenav.mode = 'side';
                this.matSidenav.toggle(true);
            }
            else
            {
                this.isLockedOpen = false;
                this.matSidenav.mode = 'over';
                this.matSidenav.toggle(false);
            }
        });
    }

    ngOnDestroy()
    {
        this.matchMediaSubscription.unsubscribe();
    }
}

@Directive({
    selector: '[webinarMatSidenavToggler]'
})
export class ProhospMatSidenavTogglerDirective
{
    @Input('webinarMatSidenavToggler') id;

    constructor(private webinarMatSidenavService: ProhospMatSidenavHelperService)
    {
    }

    @HostListener('click')
    onClick()
    {
        this.webinarMatSidenavService.getSidenav(this.id).toggle();
    }
}
