import { NgModule } from '@angular/core';

import { ProhospIfOnDomDirective } from '@prohosp/directives/webinar-if-on-dom/webinar-if-on-dom.directive';
import { ProhospPerfectScrollbarDirective } from '@prohosp/directives/webinar-perfect-scrollbar/webinar-perfect-scrollbar.directive';
import { ProhospMatSidenavHelperDirective, ProhospMatSidenavTogglerDirective } from '@prohosp/directives/webinar-mat-sidenav/webinar-mat-sidenav.directive';
import { AutoFocusDirective } from '@prohosp/directives/autofocus/autofocus.directive';

@NgModule({
    declarations: [
        ProhospIfOnDomDirective,
        ProhospMatSidenavHelperDirective,
        ProhospMatSidenavTogglerDirective,
        ProhospPerfectScrollbarDirective,
        AutoFocusDirective
    ],
    imports     : [],
    exports     : [
        ProhospIfOnDomDirective,
        ProhospMatSidenavHelperDirective,
        ProhospMatSidenavTogglerDirective,
        ProhospPerfectScrollbarDirective,
        AutoFocusDirective
    ]
})
export class ProhospDirectivesModule
{
}
