export class ApiConfigs {
    public static apiUrl = 'https://homologa.fidelize.com.br/graphql/';
    public static apiSandboxUrl = 'https://homologa.fidelize.com.br/graphql/';
    public static appUrl = 'https://app2.99webinar.com/';
    public static appSandboxUrl = 'http://localhost:4200/';
}
