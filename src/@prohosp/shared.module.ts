import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { ProhospDirectivesModule } from '@prohosp/directives/directives';
import { ProhospPipesModule } from '@prohosp/pipes/pipes.module';

@NgModule({
    imports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        ProhospDirectivesModule,
        ProhospPipesModule
    ],
    exports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        ProhospDirectivesModule,
        ProhospPipesModule
    ]
})
export class ProhospSharedModule
{
}
