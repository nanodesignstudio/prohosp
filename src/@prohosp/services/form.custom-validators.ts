import { FormControl, Validators, FormGroup } from '@angular/forms';

// setup simple regex for white listed characters
const validCharacters = /[^\s\w,.:&\/()+%'`@-]/;
const customRegexToSlugProhosp = /[a-zA-Z0-9]+((-|_)[a-zA-Z0-9]*)*/;
const customRegexToProhospVideo = /^(http[s]?:[/][/])?(www.)?((youtu)|(youtube)|(player)|(vimeo)|(daily)|(daily))([.][a-zA-Z]{2,20})+[/](.*)/;

// create your class that extends the angular validator class
export class ProhospCustomValidators extends Validators {
    
    // create a static method for your validation
    static validateCharacters(control: FormControl) {
        
        // first check if the control has a value
        if (control.value && control.value.length > 0) {
            
            // match the control value against the regular expression
            const matches = control.value.match(validCharacters);
            
            // if there are matches return an object, else return null.
            return matches && matches.length ? { invalid_characters: matches } : null;
        } else {
            return null;
        }
    }

    static slugValidator(control: FormControl) {
        return customRegexToSlugProhosp.test(control.value) ? null : {
            invalid_pattern: control.value
          };
    }

    static videoUrlValidator(control: FormControl) {
        return customRegexToProhospVideo.test(control.value) ? null : {
            video_url_invalid: control.value
          };
    }

    // Radio default value is 0 and is invalid
    static radio_required(control: FormControl) { return control.value === 0 ? { radio_required: 0 } : null; }

    static confirmPassword(passwords: FormGroup) { 
        const password = passwords.controls.NewPassword.value;
        const repeatPassword = passwords.controls.ConfirmNewPassword.value;

        if (repeatPassword.length <= 0) {
            return null;
        }
 
        if (repeatPassword !== password) {
            return {
                doesMatchPassword: true
            };
        }
 
        return null;
     }
}
