/// Service para embedar vídeos do youtube, vimeo e dailymotion
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { UrlHandlingStrategy } from '@angular/router';

@Injectable()
export class EmbedVideoService {
    
    private validYouTubeOptions = [
        'default',
        'mqdefault',
        'hqdefault',
        'sddefault',
        'maxresdefault'
    ];
    private validVimeoOptions = [
        'thumbnail_small',
        'thumbnail_medium',
        'thumbnail_large'
    ];
    private validDailyMotionOptions = [
        'thumbnail_60_url',
        'thumbnail_120_url',
        'thumbnail_180_url',
        'thumbnail_240_url',
        'thumbnail_360_url',
        'thumbnail_480_url',
        'thumbnail_720_url',
        'thumbnail_1080_url'
    ];

    constructor(
        private http: HttpClient,
        private sanitizer: DomSanitizer
    ) { }

    private detectDailymotion(url: URL) : string {
        if (url.hostname.indexOf('dailymotion.com') > -1)
            return url.pathname.split('/')[2].split('_')[0];
        if (url.hostname === 'dai.ly')
            return url.pathname.split('/')[1];
        return '';
    }

    private detectVimeo(url: URL) : string {
        return (url.hostname === 'vimeo.com') ? url.pathname.split('/')[1] : '';
    }

    private detectYoutube(url: URL) : string {
        if (url.hostname.indexOf('youtube.com') > -1)
            return url.search.split('=')[1];
        if (url.hostname === 'youtu.be')
            return url.pathname.split('/')[1];
        return '';
    }

    public embed(url: any, options?: any): any {
        url = new URL(url);
        let id: string;        
        
        id = this.detectYoutube(url);
        if (!!id)
            return this.embedYoutube(id, options);

        id = this.detectVimeo(url);
        if (!!id)
            return this.embedVimeo(id, options);

        id = this.detectDailymotion(url);
        if (!!id)
            return this.embedDailymotion(id, options);
    }
    
    private embedDailymotion(id: string, options?: any): string {
        options = this.parseOptions(options);
        let queryString;    
        if (options && options.hasOwnProperty('query')) {
            queryString = '?' + this.serializeQuery(options.query);
        }    
        return this.sanitizeIframe(`<iframe src="https://www.dailymotion.com/embed/video/${id}${options.query}" ${options.attr} frameborder="0"></iframe>`);
    }

    private embedVimeo(id: string, options?: any): string {
        options = this.parseOptions(options);
        let queryString;    
        if (options && options.hasOwnProperty('query')) {
            queryString = '?' + this.serializeQuery(options.query);
        }    
        return this.sanitizeIframe(`<iframe src="https://player.vimeo.com/video/${id}${options.query}" ${options.attr} frameborder="0"></iframe>`);
    }

    private embedYoutube(id: string, options?: any): string {
        options = this.parseOptions(options);
        let queryString: string;    
        if (options && options.hasOwnProperty('query')) {
            queryString = '?' + this.serializeQuery(options.query);
        }    
        return this.sanitizeIframe(`<iframe src="https://www.youtube.com/embed/${id}${options.query}" ${options.attr} frameborder="0"></iframe>`);
    }

    public getImage(url: any, options?: any): any {
        let id;
        url = new URL(url);
        id = this.detectYoutube(url);

        if (!!id)
            return this.getImageYoutube(id, options);

        id = this.detectVimeo(url);
        if (!!id)
            return this.getImageVimeo(id, options);

        id = this.detectDailymotion(url);
        if (!!id)
            return this.getImageDailymotion(id, options);
    }

    private getImageDailymotion(id: string, options?: any): any {
        if (typeof options === 'function')
            options = {};
        
        options = options || {};
        options.image = this.validDailyMotionOptions.indexOf(options.image) > 0 ? options.image : 'thumbnail_480_url';
        return new Promise((resolve, reject) => {
            this.http.get(`https://api.dailymotion.com/video/${id}?fields=${options.image}`).subscribe(
                (res) => resolve({
                    link: res[options.image],
                    html: `<img src="${res[options.image]}" />`
                }),
                (err) => reject(err)
            )
        });
    }

    private getImageVimeo(id: string, options?: any): any {
        if (typeof options === 'function')
            options = {};
        
        options = options || {};
        options.image = this.validVimeoOptions.indexOf(options.image) > 0 ? options.image : 'thumbnail_large';
        return new Promise((resolve, reject) => {
            this.http.get(`https://vimeo.com/api/v2/video/${id}.json`).subscribe(
                (res) => {
                    console.log('res', res, options.image, res[0][options.image]);
                    resolve({
                        link: res[0][options.image],
                        html: `<img src="${res[0][options.image]}" />`
                    });                                        
                },
                (err) => reject(err)
            )
        });
    }

    private getImageYoutube(id: string, options?: any): any {
        if (typeof options === 'function')
            options = {};
        
        options = options || {};
        options.image = this.validYouTubeOptions.indexOf(options.image) > 0 ? options.image : 'default';
        
        let src = `https://img.youtube.com/vi/${id}/${options.image}.jpg`;
        return new Promise((resolve, reject) => {
            resolve({
                link: src,
                html: `<img src="${src}" />`
            })
        });
    }

    private parseOptions(options: any): any {
        let queryString = '', attributes = '';    
        if (!!options && options.hasOwnProperty('query')) 
            queryString = '?' + this.serializeQuery(options.query);
    
        if (!!options && options.hasOwnProperty('attr')) {
            let temp = <any>[];    
            Object.keys(options.attr).forEach(function (key) {
                temp.push(key + '="' + (options.attr[key]) + '"');
            });    
            attributes = ' ' + temp.join(' ');
        }
        return {
            query: queryString,
            attr: attributes
        };
    }

    private sanitizeIframe(iframe: string): any {
        return this.sanitizer.bypassSecurityTrustHtml(iframe);
    }

    private serializeQuery(query: any): any {
        let queryString: any = [];    
        for (let p in query) {
            if (query.hasOwnProperty(p)) {
                queryString.push(encodeURIComponent(p) + '=' + encodeURIComponent(query[p]));
            }
        }    
        return queryString.join('&');
    }
}