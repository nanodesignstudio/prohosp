// Angular core
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Guards
import { AuthGuard } from './auth.guard';
// Angular Material Core
import { 
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material';
// Includes components

// ********** Account Service *********
import { AccountService } from './main/content/account/account.service';


// Prohosp
import { OrdersComponent } from './main/content/orders/orders.component';
import { ListComponent } from './main/content/orders/list/list.component';
import { EditComponent } from './main/content/orders/edit/edit.component';
import { CreateComponent } from './main/content/orders/create/create.component';
import { ResponseComponent } from './main/content/orders/response/response.component';
import { InvoiceComponent } from './main/content/orders/invoice/invoice.component';

const routes: Routes = [
    { path: '', redirectTo: 'orders/list', pathMatch: 'full' },
    {
        path        : 'orders',
        children    : [
            {
                path     : 'list',
                component: ListComponent
            },
            {
                path     : 'create',
                component: CreateComponent
            },
            {
                path     : 'response/:id',
                component: ResponseComponent
            },
            {
                path     : 'edit/:id',
                component: EditComponent
            },
            {
                path     : 'invoice/:id',
                component: InvoiceComponent
            },
        ],
    },

];

export const appRoutingProviders: any[] = [
    AccountService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
];
export const Routing = RouterModule.forRoot(routes);
