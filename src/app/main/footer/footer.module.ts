import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatIconModule, MatToolbarModule } from '@angular/material';

import { ProhospSharedModule } from '@prohosp/shared.module';

import { ProhospFooterComponent } from 'app/main/footer/footer.component';

@NgModule({
    declarations: [
        ProhospFooterComponent
    ],
    imports     : [
        RouterModule,

        MatButtonModule,
        MatIconModule,
        MatToolbarModule,

        ProhospSharedModule
    ],
    exports     : [
        ProhospFooterComponent
    ]
})
export class ProhospFooterModule
{
}
