export interface ProhospEntity {
    id: number;
    name: string;
    videoUrl: string;
    standardChat: boolean;
    thumbUrl: string;
}

export interface ChatEntity {
    id: number;    
    code: string;
    status: number;
}

export interface LeadEntity {
    id: number;
    code: string;
    webinarId: number;
    chatId: number;
    chatCode: string;
    connectionId: string;
    status: number;
    name: string;
    email: string;
    photo: string;
    avatarIndex: number;
    blocked: boolean;
    isAdmin: boolean;
    clickedOffer: boolean;
    hover: boolean;
}

export interface TimelineEntity {
    id: number;
    code: string;
    webinarId: number;
    eventType: number;
    eventStatus: number;
    eventDate: Date;
    rootId: number;
    rootCode: string;
    chatId: number;
    chatCode: string;
    leadId: number;
    leadCode: string;
    leadConnectionId: string;
    leadName: string;
    leadPhoto: string;
    leadAvatarIndex: number;
    leadAdmin: boolean;
    content: string;
    secondsFromStart: number;
    totalAnswers: number;
    answers: TimelineEntity[];
    answering: boolean;    
    hover: boolean;    
    removing: boolean;
}

export interface AlertEntity {
    id: number;
    status: number;
    content: string;
    eventDate: Date;
    read: boolean;
}

export interface JsonPatchEntity {
    op: string;
    path: string;
    value: any;
}

export interface OfferEntity {
    name: string;
    url: string;
    color: string;
}
