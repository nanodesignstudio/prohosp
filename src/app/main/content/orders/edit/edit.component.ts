// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
// Traduções
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './../orders.service'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Order } from '../order.model';


@Component({
    selector   : 'webinar-order-edit',
    templateUrl: './edit.component.html',
    styleUrls  : ['./edit.component.scss'],
    animations : webinarAnimations
})
export class EditComponent implements OnInit
{
    currentId: any;
    
    editOrder: FormGroup;
    editOrderErrors: any;
    // order: any;
    
    order = new Order();
    onOrderChanged: Subscription;
    statusForm: FormGroup;

    createResponse: boolean = false;
    // orderStatuses = orderStatuses;
    
    constructor(
        private formBuilder: FormBuilder,
        private webinarTranslationLoader: ProhospTranslationLoaderService,
        private orderService: OrdersService,
        private router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private apollo: Apollo,
    ){
        this.route.params.subscribe(params => this.order.order_code = params['id']);
        
    }
    
    ngOnInit(){
        
        if (this.order.order_code !== 0 ){
        this.onOrderChanged = this.apollo.watchQuery<any>({
            query: gql`
            query order { 
                order(
                    industry_code:"FAB"
                    order_code: ${this.order.order_code}
                ) {
                    id
                    status
                    order_code
                    industry_code
                    wholesaler_branch_code
                    customer_code
                    products {
                        ean
                        amount
                        discount_percentage
                        net_value
                        monitored
                        payment_term
                    }
                    responses{
                        id
                        imported_at
                        outcome
                    }
                    invoices{
                        id
                        imported_at
                        outcome
                    }
                    cancellations{
                        id
                        imported_at
                        outcome
                    }
                    
                }
            }
            
            `
        }).valueChanges
        .subscribe(order => {
            console.log(order.data.order);
            this.order = order.data.order;
            console.log(this.order);
        });
            // this.getOrder(this.order);
        }
        this.formsInit();
    }
    
    
    addItem(): void {
        this.products.push(this.createItem());
    }
    
    formsInit(){
        
        // Change password
        this.editOrder = new FormGroup({
            industry_code           : new FormControl('', Validators.required),
            order_code              : new FormControl('', Validators.required),
            wholesaler_code         : new FormControl('', Validators.required),
            wholesaler_order_code   : new FormControl('', Validators.required),
            payment_term            : new FormControl('', Validators.required),
            consideration           : new FormControl('', Validators.required),
            is_free_good_discount   : new FormControl('', Validators.required),
            processed_at            : new FormControl('', Validators.required),
            invoice_at              : new FormControl('', Validators.required),
            delivery_forecast_at    : new FormControl('', Validators.required),
            products                : this.formBuilder.array([this.createItem()]),
        });
        
        this.editOrderErrors = {
            industry_code           : '',
            order_code              : '',
            wholesaler_code         : '',
            wholesaler_order_code   : '',
            payment_term            : '',
            consideration           : '',
            is_free_good_discount   : '',
            processed_at            : '',
            invoice_at              : '',
            delivery_forecast_at    : '',
            products                : '',
        };
    }
    
    getOrder(orderId){
        let query = this.apollo.watchQuery<any>({
            query: gql`
            query order { 
                order(
                    industry_code:"FAB"
                    order_code: ${orderId}
                ) {
                    id
                    status
                    order_code
                    industry_code
                    wholesaler_branch_code
                    customer_code
                    products {
                        ean
                        amount
                        discount_percentage
                        net_value
                        monitored
                        payment_term
                    }
                    responses{
                        id
                        imported_at
                        outcome
                    }
                    invoices{
                        id
                        imported_at
                        outcome
                    }
                    cancellations{
                        id
                        imported_at
                        outcome
                    }
                    
                }
            }
            
            `
        })
        .valueChanges
        .subscribe(({ data, loading }) => {
            console.log(data)
            // if (imported){
            //     this.orders = data.orders.data;
            //     this.importedOrders = data.orders.data;
            //     localStorage.setItem('importedOrders', JSON.stringify(data.orders));
            // } else{
            
            //     this.loading = loading;
            //     this.orders = data.orders.data;
            //     this.notImportedOrders = this.orders;
            //     localStorage.setItem('orders', JSON.stringify(data.orders));
            // }
        });
    }
    
    createItem(): FormGroup {
        return  this.formBuilder.group({
            ean                         : new FormControl('', Validators.required),
            invoice_ean                 : new FormControl('', Validators.required),
            response_amount             : new FormControl('', Validators.required),
            unit_discount_percentage    : new FormControl('', Validators.required),
            unit_discount_value         : new FormControl('', Validators.required),
            unit_net_value              : new FormControl('', Validators.required),
            monitored                   : new FormControl('', Validators.required),
            industry_consideration      : new FormControl('', Validators.required),
            wholesaler_consideration    : new FormControl('', Validators.required),
        })
    }

    loadresponse(order){
        this.editOrder.patchValue([ this.editOrder ])
    }
    
    get products(): FormArray {
        return this.editOrder.get('products') as FormArray;
    };
    
    public showError(error: string = '') {
        this.snackBar.open(error, '', {
            duration: 5000,
            panelClass: 'snackbar-error',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
    public showSuccess(message: string = '') {
        this.snackBar.open(message, '', {
            duration: 5000,
            panelClass: 'snackbar-success',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
}
