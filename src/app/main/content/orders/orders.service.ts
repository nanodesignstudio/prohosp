import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';

import { ApiConfigs } from '@prohosp/configurations/api.config';
import { AccountService } from '../account/account.service';
import { Environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrdersService
{
    apiUrl: string;

    constructor(
        private http: HttpClient,
        private accountService: AccountService,
        private apollo: Apollo,
    )
    {
        this.apiUrl = Environment.development ?
        ApiConfigs.apiSandboxUrl :
        ApiConfigs.apiUrl;
    }

    getOrders(){
        return new Promise((resolve, reject) => {
            this.http.get('http://127.0.0.1/prohosp/api.php/pedidos?order=id,desc&transform=1' ).subscribe((Response) => {
                resolve(Response);
            }, reject);
        });
    }

    getOrder(){
        return {query: gql`
        query orders { 
            orders(
                current_page:1
                per_page:10
                imported:false
            ) {
                total
                from
                to
                data{
                    id
                    status
                    order_code
                    industry_code
                    customer_code
                    customer_alternative_code
                    distribution_center_code
                    products{
                        ean
                        amount
                        discount_percentage
                        net_value
                        monitored
                        payment_term
                        
                    }
                }
            }
        }
        `};
    }

    insertOrder(data: any) {
        const json = {
            order_id: data.createGroupedOrder.grouped_order_code,
            response: JSON.stringify(data)
        };
        return new Promise((resolve, reject) => {
            this.http.post('http://127.0.0.1/prohosp/api.php/pedidos', json ).subscribe((Response) => {
                resolve(Response);
            }, reject);
        });
    }

    removeProhosp(id: number) {
        return new Promise((resolve, reject) => {
            this.http.delete(this.apiUrl + 'api/webinar/' + id, {
                headers: new HttpHeaders({
                    'Authorization': 'Bearer ' + this.accountService.getApiToken()
                })
            }).subscribe((Response) => {
                resolve(Response);
            }, reject);
        });
    }

}
