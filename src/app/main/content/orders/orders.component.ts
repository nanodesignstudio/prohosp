// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild, ChangeDetectorRef  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './orders.service';
import { AccountService } from '../account/account.service';
// import { RoomService } from '../room/room.service';
import { ProhospCopierService } from '@prohosp/services/copier.service';
// Add Prohosp
// import { ConfigureProhospDialogComponent } from '../room/includes/configure/configure.component';
import { ConsoleLogger } from '@aspnet/signalr';
import { ProhospEntity } from 'app/main/models/webinar.interface';
import { Subscription } from 'apollo-client/util/Observable';



@Component({
    selector   : 'webinar-orders',
    templateUrl: './orders.component.html',
    styleUrls  : ['./orders.component.scss'],
    animations : webinarAnimations
})
export class OrdersComponent implements OnInit
{
    confirmDialogRef: MatDialogRef<ProhospConfirmDialogComponent>;
    orders: any;
    error: string;
    
    notImportedOrders: any;
    importedOrders: any;
    
    displayedColumns = ['order_code', 'status',  'customer_code', 'distribution_center_code', 'buttons'];
    
    loading: boolean;
    
    imported: boolean = false;
    
    private querySubscription: Subscription;
    
    constructor(
        // private webinarCreateComponent: ConfigureProhospDialogComponent,
        private ordersService: OrdersService,
        public snackBar: MatSnackBar,
        private dialog: MatDialog,
        private router: Router,
        private copier: ProhospCopierService,
        private accountService: AccountService,
        // private roomService: RoomService,
        private apollo: Apollo,
        private cdr: ChangeDetectorRef
    )
    {
        
        this.loading = true;
    }
    
    ngOnInit()
    {
        this.reloadOrders();
    }

    reloadOrders(){
        this.getOrders(true);
        this.getOrders(false);
        this.cdr.detectChanges();
    }
    
    copyLink(webinar){
        const user = this.accountService.getUser();
        // const link = 'https://app2.99webinar.com/live/' + user.slug + '/' + webinar.webinarUrl;
        const link = 'https://';
        
        if ( this.copier.copyText(link) ){
            this.showSuccess('Link copiado');
        }else{
            this.showError('Ops, não deu certo. Tente novamente!');
        }
    }  
    
    getOrders(imported: boolean = true) {

        this.querySubscription = this.apollo.watchQuery<any>({
            query: gql`
            query orders { 
                orders(
                    current_page:1
                    per_page:10
                    imported: ${imported}
                ) {
                    total
                    from
                    to
                    data{
                        id
                        status
                        order_code
                        industry_code
                        customer_code
                        customer_alternative_code
                        distribution_center_code
                        products{
                            ean
                            amount
                            discount_percentage
                            net_value
                            monitored
                            payment_term
                            
                        }
                    }
                }
            }   
            `
        })
        .valueChanges
        .subscribe(({ data, loading }) => {
            if (imported){
                this.orders = data.orders.data;
                this.importedOrders = data.orders.data;
                localStorage.setItem('importedOrders', JSON.stringify(data.orders));
            } else{
                
                this.loading = loading;
                this.orders = data.orders.data;
                this.notImportedOrders = this.orders;
                localStorage.setItem('orders', JSON.stringify(data.orders));
            }
        });
    }
    
    importOrder(order){
        this.querySubscription = this.apollo.mutate<any>({
            mutation: gql`
            mutation setOrderAsImported{
                setOrderAsImported(
                  id: ${order.id}
                )
                {
                  id
                  order_code
                  customer_code
                  customer_alternative_code
                }
              }
               
            `
        })
        .subscribe(({ data }) => {

                this.loading = false;
                if (data.setOrderAsImported){
                    this.showSuccess('Importado');
                    window.location.reload();
                } else {
                    this.showError('Houve algum erro');
                }
                console.log(data);
                localStorage.setItem('orders', JSON.stringify(data.orders));
        });
    }
    
    public showError(error: string = '') {
        this.snackBar.open(error, '', {
            duration: 5000,
            panelClass: 'snackbar-error',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
    public showSuccess(message: string = '') {
        this.snackBar.open(message, '', {
            duration: 5000,
            panelClass: 'snackbar-success',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
}


export class ExampleDataSource extends DataSource<any> {
    constructor(private _exampleDatabase: AccountService) {
        super();
    }
    
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any> {
        let x = JSON.parse(localStorage.getItem('orders'));
        return x.data;
    }
    
    disconnect() {}
}