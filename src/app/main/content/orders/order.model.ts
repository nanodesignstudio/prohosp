import { ProhospUtils } from '@prohosp/utils'

export class Order {
    industry_code:string;
    order_code:number;
    wholesaler_code:string;
    wholesaler_order_code:string;
    payment_term:string;
    consideration:string;
    is_free_good_discount:string;
    processed_at:string;
    invoice_at:string;
    delivery_forecast_at:string;
    products: any[];
    responses: any[];
    invoices: any[];
    cancelations: any[];

    constructor(order?)
    {
        console.log(order);
        order = order || {};
        this.industry_code = order.industry_code || '';
        this.order_code = order.order_code || 0;
        this.wholesaler_code = order.wholesaler_code || '';
        this.wholesaler_order_code = order.wholesaler_order_code || '';
        this.payment_term = order.payment_term || '';
        this.consideration = order.consideration || '';
        this.is_free_good_discount = order.is_free_good_discount || '';
        this.processed_at = order.processed_at || '';
        this.invoice_at = order.invoice_at || '';
        this.delivery_forecast_at = order.delivery_forecast_at || '';
        this.products = order.products || [];
        this.responses = order.responses || [];
        this.invoices = order.invoices || [];
        this.cancelations = order.cancelations || [];
    }
}



export class GroupedPreOrder {
    client_identification: string;
    wholesaler: string;
    client_code: string;
    commercial_condition: string;
    products:  any[];
}
