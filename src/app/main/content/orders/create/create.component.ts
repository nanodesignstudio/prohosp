// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
// Traduções
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './../orders.service'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Order, GroupedPreOrder } from '../order.model';


@Component({
    selector   : 'webinar-order-create',
    templateUrl: './create.component.html',
    styleUrls  : ['./create.component.scss'],
    animations : webinarAnimations
})
export class CreateComponent implements OnInit
{
    currentId: any;
    
    createOrder: FormGroup;
    createOrderErrors: any;
    inCreate: boolean = false;
    // order: any;
    
    order = new GroupedPreOrder();
    onOrderChanged: Subscription;
    statusForm: FormGroup;

    produtos: any;
    
    createResponse: boolean = false;
    // orderStatuses = orderStatuses;
    
    constructor(
        private formBuilder: FormBuilder,
        private webinarTranslationLoader: ProhospTranslationLoaderService,
        private orderService: OrdersService,
        private router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private apollo: Apollo,
    ){        
    }
    
    ngOnInit(){
        
        this.formsInit();

        this.produtos = [
            {ean: '7891058003203', label:'(7949) CLEXANE 20MG 10SER SAFETY LOCK'},
            {ean: '7896070605268', label:'(7740) CLEXANE 40MG 10SER SAFETY LOCK'},
            {ean: '7891058003241', label:'(7741) CLEXANE 60MG 2SER SAFETY LOCK '},
            {ean: '7891058003272', label:'(7707) CLEXANE 80MG 2SER SAFETY LOCK '},
            {ean: '7891058003302', label:'(8020) CLEXANE 100MG 2SER SAFETY LOCK'},
        ];
    }
    
    addItem(): void {
        this.products.push(this.createItem());
    }
    
    formsInit(){
        
        // Change password
        this.createOrder = new FormGroup({
            client_identification   : new FormControl('', Validators.required),
            wholesaler              : new FormControl('', Validators.required),
            client_code             : new FormControl('', Validators.required),
            commercial_condition    : new FormControl(' ', Validators.required),
            products: this.formBuilder.array([this.createItem()]),
        });
        
        this.createOrderErrors = {
            client_identification   : '',
            wholesaler              : '',
            client_code             : '',
            commercial_condition    : '',
            products                : '',
        };
    }


    createPreOrder(){
        this.inCreate = true;
        let products = JSON.stringify(this.createOrder.get('products').value);
        console.log(products.replace(/("ean")/g, 'ean'));
        products = products.replace(/("ean")/g, 'ean');
        products = products.replace(/("ordered_quantity")/g, 'ordered_quantity');
        products = products.replace(/("wholesaler_discount")/g, 'wholesaler_discount');
        let mutation = this.apollo.mutate({
            mutation: gql`
            mutation createGroupedOrder{createGroupedOrder(
                client_identification: "${this.createOrder.value.client_identification}",
                wholesaler: "${this.createOrder.value.wholesaler}",
                client_code: "${this.createOrder.value.client_code}",
                commercial_condition: "${this.createOrder.value.commercial_condition}",
                products: ${products}
            ) {
                
                grouped_order_code
                client_identification
                wholesaler
                client_code
                commercial_condition
                status
                total_products
            } 
        }
        
        `
    }).subscribe(({data}) => {
        this.inCreate = false;
        console.log('data', data);
        this.orderService.insertOrder(data).then((response) => {
            console.log('Foi', response);
            if ( data.createGroupedOrder ){
                this.showSuccess('Pedido criado com sucesso!');
                this.router.navigateByUrl('/orders/list');
            } else{
                this.showError('Oops, aconteceu algo inesperado.');
            }
        });
    });
}

getOrder(orderId){
    let query = this.apollo.watchQuery<any>({
        query: gql`
        query order { 
            order(
                industry_code:"FAB"
                order_code: ${orderId}
            ) {
                id
                status
                order_code
                industry_code
                wholesaler_branch_code
                customer_code
                products {
                    ean
                    amount
                    discount_percentage
                    net_value
                    monitored
                    payment_term
                }
                responses{
                    id
                    imported_at
                    outcome
                }
                invoices{
                    id
                    imported_at
                    outcome
                }
                cancellations{
                    id
                    imported_at
                    outcome
                }
                
            }
        }
        
        `
    })
    .valueChanges
    .subscribe(({ data, loading }) => {
        console.log(data);
        // if (imported){
        //     this.orders = data.orders.data;
        //     this.importedOrders = data.orders.data;
        //     localStorage.setItem('importedOrders', JSON.stringify(data.orders));
        // } else{
        
        //     this.loading = loading;
        //     this.orders = data.orders.data;
        //     this.notImportedOrders = this.orders;
        //     localStorage.setItem('orders', JSON.stringify(data.orders));
        // }
    });
}

createItem(): FormGroup {
    return  this.formBuilder.group({
        ean                         : new FormControl('', Validators.required),
        ordered_quantity            : new FormControl(0, Validators.required),
        wholesaler_discount         : new FormControl(0, Validators.required),
    });
}

loadresponse(order){
    this.createOrder.patchValue([ this.createOrder ]);
}

get products(): FormArray {
    return this.createOrder.get('products') as FormArray;
}

public showError(error: string = '') {
    this.snackBar.open(error, '', {
        duration: 5000,
        panelClass: 'snackbar-error',
        horizontalPosition: 'left',
        verticalPosition: 'bottom'
    });
}

public showSuccess(message: string = '') {
    this.snackBar.open(message, '', {
        duration: 5000,
        panelClass: 'snackbar-success',
        horizontalPosition: 'left',
        verticalPosition: 'bottom'
    });
}
}
