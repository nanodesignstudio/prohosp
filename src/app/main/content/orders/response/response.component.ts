// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
import { DatePipe } from '@angular/common';

// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
// Traduções
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './../orders.service'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Order, GroupedPreOrder } from '../order.model';
import { debounceTime } from 'rxjs/operator/debounceTime';
import { distinctUntilChanged } from 'rxjs/operator/distinctUntilChanged';


@Component({
    selector   : 'webinar-order-response',
    templateUrl: './response.component.html',
    styleUrls  : ['./response.component.scss'],
    animations : webinarAnimations
})
export class ResponseComponent implements OnInit
{
    currentId: any;
    
    responseOrder: FormGroup;
    responseOrderErrors: any;
    
    editOrder: FormGroup;
    editOrderErrors: any;
    // order: any;
    
    order:  any;
    onOrderChanged: Subscription;
    statusForm: FormGroup;
    
    responseResponse: boolean = false;
    // orderStatuses = orderStatuses;
    
    productsForResponse: any;
    
    constructor(
        private formBuilder: FormBuilder,
        private webinarTranslationLoader: ProhospTranslationLoaderService,
        private orderService: OrdersService,
        private router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private apollo: Apollo,
    ){        
        
        this.route.params.subscribe(params => this.currentId = params['id']);
        
    }
    
    ngOnInit(){
        this.formsInit();
        this.getOrder(this.currentId);
        this.responseOrder.valueChanges.subscribe((val)=> {
            val.products.forEach(element => {
                element.unit_discount_price = (element.order_discount * element.unit_net_price) / 100;
            });
        });
    }
    
    
    addItem(): void {
        this.products.push(this.responseItem());
    }
    
    formsInit(){
        
        // Change password
        this.responseOrder = new FormGroup({
            date_time_processing     : new FormControl('', Validators.required),
            id                       : new FormControl('', Validators.required),
            order_motive             : new FormControl('', Validators.required),
            products: this.formBuilder.array([this.responseItem()]),
            total_value             : new FormControl('', Validators.required),
            discount_value          : new FormControl('', Validators.required),
        });
        
        this.responseOrderErrors = {
            date_time_processing   : '',
            wholesaler              : '',
            client_code             : '',
            commercial_condition    : '',
            products                : '',
        };
        
        
        // Change password
        this.editOrder = new FormGroup({
            industry_code           : new FormControl('', Validators.required),
            order_code              : new FormControl('', Validators.required),
            wholesaler_code         : new FormControl('', Validators.required),
            wholesaler_order_code   : new FormControl('', Validators.required),
            payment_term            : new FormControl('', Validators.required),
            consideration           : new FormControl('', Validators.required),
            is_free_good_discount   : new FormControl('', Validators.required),
            processed_at            : new FormControl('', Validators.required),
            invoice_at              : new FormControl('', Validators.required),
            delivery_forecast_at    : new FormControl('', Validators.required),
            product_reason          : new FormControl('', Validators.required),
            total_value             : new FormControl('', Validators.required),
            discount_value          : new FormControl('', Validators.required),
            products                : this.formBuilder.array([this.responseItem()]),
        });
        
        this.editOrderErrors = {
            industry_code           : '',
            order_code              : '',
            wholesaler_code         : '',
            wholesaler_order_code   : '',
            payment_term            : '',
            consideration           : '',
            is_free_good_discount   : '',
            processed_at            : '',
            invoice_at              : '',
            delivery_forecast_at    : '',
            products                : '',
        };
    }
    
    
    
    getOrder(orderId){
        let query = this.apollo.watchQuery<any>({
            query: gql`
            query groupedOrder{
                groupedOrder(
                    id: ${orderId}
                ) {
                    id
                    grouped_order_code
                    client_identification
                    wholesaler
                    client_code
                    commercial_condition
                    status
                    total_products
                    products
                    {
                        ean
                        ordered_quantity
                        wholesaler_discount
                        order_discount
                        unit_net_price
                        industry_order_code
                        product_reason
                    }
                }
            }
            
            `
        })
        .valueChanges
        .subscribe(({ data, loading }) => {
            this.order = data.groupedOrder;
            for (let i = 1; i < data.groupedOrder.products.length; i++) {
                this.addItem();
            }
            this.responseOrder.patchValue(
                {products: data.groupedOrder.products}
            );
        });
    }
    
    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    
    createResponse(){
        let products = this.responseOrder.get('products').value;
        for(var i = 0; i < products.length; i++) {
            delete products[i]['order_discount'];
            delete products[i]['wholesaler_discount'];
            // delete products[i]['ordered_quantity'];
            delete products[i]['industry_order_code'];
            // delete products[i]['response_quantity'];
            products[i]['product_reason'] = "*remove*PRODUCT_SUCCESSFULLY_ACCEPTED*remove*";
            // products[i]['response_quantity'] = parseInt(products[i]['response_quantity']);
        }
        
        products = JSON.stringify(products);
        
        products = products.replace(/("ean")/g, 'ean');
        products = products.replace(/("ordered_quantity")/g, 'response_quantity');
        products = products.replace(/("wholesaler_discount")/g, 'wholesaler_discount');
        
        products = products.replace(/("response_quantity")/g, 'response_quantity');
        products = products.replace(/("percent_discount")/g, 'percent_discount');
        products = products.replace(/("unit_discount_price")/g, 'unit_discount_price');
        products = products.replace(/("unit_net_price")/g, 'unit_net_price');
        products = products.replace(/("product_reason")/g, 'product_reason');
        products = products.replace(/("monitored")/g, 'monitored');
        products = products.replace(/("wholesaler_reason")/g, 'wholesaler_reason');
        products = products.replace(/("order_discount")/g, 'order_discount');
        products = products.replace(/("industry_order_code")/g, 'industry_order_code');
        products = products.replace(/("\*remove\*)/g, '');
        products = products.replace(/(\*remove\*")/g, '');
        
        let date = new Date();
        let d = this.addZero(date.getDate());
        let m = this.addZero(date.getMonth() + 1);
        let y = this.addZero(date.getFullYear());
        let hou = this.addZero(date.getHours());
        let min = this.addZero(date.getMinutes());
        let sec = this.addZero(date.getSeconds());
        
        var dateNow = y + '-' + m + '-' + d + ' ' + hou + ':' + min + ':' + sec;
        
        console.log(`mutation createGroupedResponse{ 
            createGroupedResponse(
                date_time_processing:"${dateNow}", 
                id: ${this.order.id}, 
                order_motive: ORDER_SUCCESSFULLY_ACCEPTED, 
                products: ${products},
                total_value: ${this.responseOrder.value.total_value},
                discount_value: ${this.responseOrder.value.discount_value}
            ) {
                grouped_order_code
                client_identification
                wholesaler
                client_code
                commercial_condition
                status
                total_products
                products {
                    ean
                    ordered_quantity
                    wholesaler_discount
                    order_discount
                    unit_net_price
                    industry_order_code
                    product_reason
                    industry_abbreviation
                }
            }
        }
        `)
        
        let query = this.apollo.mutate({
            mutation: gql`
            mutation createGroupedResponse{ 
                createGroupedResponse(
                    date_time_processing:"${dateNow}", 
                    id: ${this.order.id}, 
                    order_motive: ORDER_SUCCESSFULLY_ACCEPTED, 
                    products: ${products},
                    total_value: ${this.responseOrder.value.total_value},
                    discount_value: ${this.responseOrder.value.discount_value}
                ) {
                    grouped_order_code
                    client_identification
                    wholesaler
                    client_code
                    commercial_condition
                    status
                    total_products
                    products {
                        ean
                        ordered_quantity
                        wholesaler_discount
                        order_discount
                        unit_net_price
                        industry_order_code
                        product_reason
                        industry_abbreviation
                    }
                }
            }
            
            `
        })
        .subscribe(({ data }) => {
            this.order = data.createGroupedResponse;
            // for (let i = 1; i < data.groupedOrder.products.length; i++) {
            //     this.addItem();
            // }
            // this.responseOrder.patchValue(
            //     {products: data.groupedOrder.products}
            // );
            
            if( data.createGroupedResponse.grouped_order_code === parseInt(this.currentId)){
                this.showSuccess('Pedido criado com sucesso!');
                this.router.navigateByUrl('/orders/list');
            } else{
                this.showError('Oops, aconteceu algo inesperado.');
            }
            // if (imported){
            //     this.orders = data.orders.data;
            //     this.importedOrders = data.orders.data;
            //     localStorage.setItem('importedOrders', JSON.stringify(data.orders));
            // } else{
            
            //     this.loading = loading;
            //     this.orders = data.orders.data;
            //     this.notImportedOrders = this.orders;
            //     localStorage.setItem('orders', JSON.stringify(data.orders));
            // }
        });
    }
    
    responseItem(): FormGroup {
        return  this.formBuilder.group({
            ean : new FormControl( Validators.required),
            response_quantity : new FormControl(Validators.required),
            percent_discount : new FormControl(Validators.required),
            unit_discount_price : new FormControl(Validators.required),
            product_reason : 'PRODUCT_SUCCESSFULLY_ACCEPTED',
            monitored : new FormControl(true, Validators.required),
            wholesaler_reason : new FormControl('PRODUCT_SUCCESSFULLY_ACCEPTED', Validators.required),
            
            unit_net_price : new FormControl( Validators.required),
            order_discount:  new FormControl( Validators.required),
            wholesaler_discount:  new FormControl( Validators.required),
            ordered_quantity:  new FormControl( Validators.required),
            industry_order_code:  new FormControl( Validators.required),
        });
    }
    
    loadresponse(order){
        this.responseOrder.patchValue([ this.responseOrder ]);
    }
    
    get products(): FormArray {
        return this.responseOrder.get('products') as FormArray;
    }
    
    public showError(error: string = '') {
        this.snackBar.open(error, '', {
            duration: 5000,
            panelClass: 'snackbar-error',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
    public showSuccess(message: string = '') {
        this.snackBar.open(message, '', {
            duration: 5000,
            panelClass: 'snackbar-success',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
}
