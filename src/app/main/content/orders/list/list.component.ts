// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
// Traduções
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './../orders.service'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Order, GroupedPreOrder } from '../order.model';

import { AccountService } from '../../account/account.service';


@Component({
    selector   : 'webinar-order-list',
    templateUrl: './list.component.html',
    styleUrls  : ['./list.component.scss'],
    animations : webinarAnimations
})
export class ListComponent implements OnInit
{
    confirmDialogRef: MatDialogRef<ProhospConfirmDialogComponent>;
    orders: any;
    error: string;
    
    notImportedOrders: any;
    importedOrders: any[] = [];
    
    displayedColumns = ['order_code', 'status',  'customer_code', 'distribution_center_code', 'buttons'];
    
    loading: boolean;
    
    imported: boolean = false;
    
    private querySubscription: Subscription;
    
    constructor(
        private ordersService: OrdersService,
        public snackBar: MatSnackBar,
        private dialog: MatDialog,
        private router: Router,
        private accountService: AccountService,
        private apollo: Apollo,
    )
    {
        
        this.loading = true;
    }
    
    ngOnInit()
    {
        this.reloadOrders();
    }
    
    reloadOrders(){
        this.getOrders();
    }

    cancellationOrder(order){
        let product: any[] = [];
        let allProducts = order.products;
        allProducts.forEach(element => {
            product.push( {ean: element.ean} );
        });
        // product.replace('"ean"', 'ean');
            let query = this.apollo.mutate<any>({
                mutation: gql`
                mutation createGroupedCancellation{
                    createGroupedCancellation(
                      id: ${order.id}
                      products: ${JSON.stringify(product).replace(/\"ean\"/g, 'ean')}
                    )
                    {
                      id
                      status
                     }
                    }
                   
                `
            })
            .subscribe(({ data }) => {
                
                this.showSuccess('Pedido cancelado com sucesso');
               
            });
    }
    
    getOrders() {
        
        const orders = this.ordersService.getOrders().then((result: any) => {
            console.log('resultado', result);
            const results = result.pedidos;
            results.forEach((element: any) => {
                if (! element.ignore_order ){
                    // console.log('elemento', element.order_id);
                    this.getOrder( element.order_id );
                    // console.log(this.importedOrders.length);
                }
                });
        });
        // this.querySubscription = this.apollo.watchQuery<any>({
        //     query: gql`
        //     query orders { 
        //         orders(
        //             current_page:1
        //             per_page:10
        //             imported: ${imported}
        //         ) {
        //             total
        //             from
        //             to
        //             data{
        //                 id
        //                 status
        //                 order_code
        //                 industry_code
        //                 customer_code
        //                 customer_alternative_code
        //                 distribution_center_code
        //                 products{
        //                     ean
        //                     amount
        //                     discount_percentage
        //                     net_value
        //                     monitored
        //                     payment_term
        
        //                 }
        //             }
        //         }
        //     }   
        //     `
        // })
        // .valueChanges
        // .subscribe(({ data, loading }) => {
        //     if (imported){
        //         this.orders = data.orders.data;
        //         this.importedOrders = data.orders.data;
        //         localStorage.setItem('importedOrders', JSON.stringify(data.orders));
        //     } else{
        
        //         this.loading = loading;
        //         this.orders = data.orders.data;
        //         this.notImportedOrders = this.orders;
        //         localStorage.setItem('orders', JSON.stringify(data.orders));
        //     }
        // });
        
    }
    
    getOrder(orderId){
        let query = this.apollo.watchQuery<any>({
            query: gql`
            query groupedOrder{
                groupedOrder(
                    id: ${orderId}
                ) {
                    id
                    grouped_order_code
                    client_identification
                    wholesaler
                    client_code
                    commercial_condition
                    status
                    total_products
                    products
                    {
                        ean
                        ordered_quantity
                        wholesaler_discount
                        order_discount
                        unit_net_price
                        industry_order_code
                        product_reason
                    }
                }
            }
            
            `
        })
        .valueChanges
        .subscribe(({ data, loading }) => {
            this.importedOrders.push(data.groupedOrder);
            console.log(this.importedOrders);

                this.importedOrders.sort((a, b) => (b.id - a.id));
           
            // if (imported){
            //     this.orders = data.orders.data;
            //     this.importedOrders = data.orders.data;
            //     localStorage.setItem('importedOrders', JSON.stringify(data.orders));
            // } else{
            
            //     this.loading = loading;
            //     this.orders = data.orders.data;
            //     this.notImportedOrders = this.orders;
            //     localStorage.setItem('orders', JSON.stringify(data.orders));
            // }
        });
    }
    
    importOrder(order){
        this.querySubscription = this.apollo.mutate<any>({
            mutation: gql`
            mutation setOrderAsImported{
                setOrderAsImported(
                    id: ${order.id}
                )
                {
                    id
                    order_code
                    customer_code
                    customer_alternative_code
                }
            }
            
            `
        })
        .subscribe(({ data }) => {
            
            this.loading = false;
            if (data.setOrderAsImported){
                this.showSuccess('Importado');
                window.location.reload();
            } else {
                this.showError('Houve algum erro');
            }
            console.log(data);
            localStorage.setItem('orders', JSON.stringify(data.orders));
        });
    }
    
    public showError(error: string = '') {
        this.snackBar.open(error, '', {
            duration: 5000,
            panelClass: 'snackbar-error',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
    public showSuccess(message: string = '') {
        this.snackBar.open(message, '', {
            duration: 5000,
            panelClass: 'snackbar-success',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
}


export class ExampleDataSource extends DataSource<any> {
    constructor(private _exampleDatabase: AccountService) {
        super();
    }
    
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any> {
        let x = JSON.parse(localStorage.getItem('orders'));
        return x.data;
    }
    
    disconnect() {}
}
