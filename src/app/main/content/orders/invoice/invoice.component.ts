// Core do angular
import { Component, OnInit, Inject, ViewEncapsulation, ViewChild  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Router, ActivatedRoute, Route } from '@angular/router';
// Core do sistema
import { ProhospConfigService } from '@prohosp/services/config.service';
import { webinarAnimations } from '@prohosp/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';


import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
// Traduções
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
// Customizações
import { ProhospConfirmDialogComponent } from '@prohosp/components/confirm-dialog/confirm-dialog.component';
import { OrdersService } from './../orders.service'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Order, GroupedPreOrder } from '../order.model';

import { AccountService } from '../../account/account.service';


@Component({
    selector   : 'webinar-order-invoice',
    templateUrl: './invoice.component.html',
    styleUrls  : ['./invoice.component.scss'],
    animations : webinarAnimations
})
export class InvoiceComponent implements OnInit
{
    currentId: any;
    
    responseOrder: FormGroup;
    responseOrderErrors: any;
    
    invoiceForm: FormGroup;
    invoiceFormErrors: any;
    // order: any;
    
    order:  any;
    onOrderChanged: Subscription;
    statusForm: FormGroup;
    
    responseResponse: boolean = false;
    // orderStatuses = orderStatuses;
    
    productsForResponse: any;
    
    dateNow: any;
    dateTimeNow: any;
    
    advancedInvoice: any;
    
    
    constructor(
        private formBuilder: FormBuilder,
        private webinarTranslationLoader: ProhospTranslationLoaderService,
        private orderService: OrdersService,
        private router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private apollo: Apollo,
    ){        
        let date = new Date();
        let d = this.addZero(date.getDate());
        let m = this.addZero(date.getMonth()  + 1 );
        let y = this.addZero(date.getFullYear());
        let hou = this.addZero(date.getHours());
        let min = this.addZero(date.getMinutes());
        let sec = this.addZero(date.getSeconds());
        
        this.dateTimeNow = y + '-' + m + '-' + d + ' ' + hou + ':' + min + ':' + sec;
        this.dateNow = y + '-' + m + '-' + d ;
        
        this.route.params.subscribe(params => this.currentId = params['id']);
    }
    
    ngOnInit(){
        this.formsInit();
        this.getOrder(this.currentId);
    }
    
    
    addItem(): void {
        this.invoice_products.push(this.invoiceItem());
    }
    
    
    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    
    formsInit(){
        
        // Change password
        this.invoiceForm = new FormGroup({
            invoice_issue_date                      : new FormControl(this.dateNow, Validators.required),
            date_time_processing                    : new FormControl(this.dateTimeNow, Validators.required),
            id                                      : new FormControl({value: this.currentId, disabled: true}, Validators.required),
            invoice_number                          : new FormControl(Validators.required),
            base_calculation_icms_tax_substitution  : new FormControl(),
            base_calculation_icms                   : new FormControl(),
            danfe                                   : new FormControl('', Validators.required),
            invoice_value                           : new FormControl(Validators.required),
            invoice_discount_value                  : new FormControl(Validators.required),
            icms_value_transferred                  : new FormControl(),
            products_total_value                    : new FormControl(Validators.required),
            invoice_products                        : this.formBuilder.array([this.invoiceItem()]),
            icms_total_value_withheld               : new FormControl(),
            icms_total_value                        : new FormControl(),
            volume_quantity                         : new FormControl(),
        });
        
        this.invoiceFormErrors = {
            invoice_issue_date                      : '',
            date_time_processing                    : '',
            id                                      : '',
            invoice_number                          : '',
            base_calculation_icms_tax_substitution  : '',
            base_calculation_icms                   : '',
            danfe                                   : '',
            invoice_value                           : '',
            invoice_discount_value                  : '',
            icms_value_transferred                  : '',
            products_total_value                    : '',
            invoice_products                        : '',
        };
    }
    
    
    
    getOrder(orderId){
        let query = this.apollo.watchQuery<any>({
            query: gql`
            query groupedOrder{
                groupedOrder(
                    id: ${orderId}
                ) {
                    id
                    grouped_order_code
                    client_identification
                    wholesaler
                    client_code
                    commercial_condition
                    status
                    total_products
                    products
                    {
                        ean
                        ordered_quantity
                        wholesaler_discount
                        order_discount
                        unit_net_price
                        industry_order_code
                        product_reason
                    }
                }
            }
            
            `
        })
        .valueChanges
        .subscribe(({ data, loading }) => {
            let productsPush: any[] = [];
            this.order = data.groupedOrder;
            for (let i = 0; i < data.groupedOrder.products.length; i++) {
                console.log(i);
                console.log(data.groupedOrder.products[i].ean);
                if ( i !== 0){
                    this.addItem();
                }
                productsPush.push(
                    {
                        ean: data.groupedOrder.products[i].ean,
                        invoice_quantity: data.groupedOrder.products[i].ordered_quantity,
                        unit_net_price: data.groupedOrder.products[i].unit_net_price
                    }
                );
            }
            this.invoiceForm.patchValue(
                {invoice_products: productsPush}
            );
            
            console.log(this.invoiceForm.value);
        });
    }
    
    invoiceItem(): FormGroup {
        return  this.formBuilder.group({
            ean                                     : new FormControl('', Validators.required),
            invoice_quantity                        : new FormControl( Validators.required),    // 1,
            percent_discount                        : new FormControl( Validators.required),    // 1.87,
            unit_discount_price                     : new FormControl(),    // 2.30,
            unit_net_price                          : new FormControl(),    // 10.38,
            base_calculation_icms                   : new FormControl(),    // 5.30,
            base_calculation_icms_tax_substitution  : new FormControl(),    // 4.23,
            percent_icms_aliquot                    : new FormControl(),    // 1.39,
            percent_ipi_aliquot                     : new FormControl(),    // 1.23,
            icms_value                              : new FormControl(),    // 5.00,
            st_value                                : new FormControl(),    // 1.39,
            icms_transferred_value                  : new FormControl(),    // 3.98,
            cfop                                    : new FormControl(),    // 10.55,
            tax_substitution_flag                   : new FormControl(''),    // "O",
            positive_list_identifier                : new FormControl(''),    // "P",
            product_deadline                        : new FormControl(),    // 90,
            product_classification                  : new FormControl(),    // 2,
            dcb                                     : new FormControl(''),    // "abcd",
            transfer_value                          : new FormControl(),    // 5.44,
            product_total_value                     : new FormControl(),    // 10.93,
            product_total_value_charges             : new FormControl(),    // 19.10,
            tax_status_code                         : new FormControl(''),    // "D",
            percent_financial_discount              : new FormControl(),    // 10.18,
            percent_unit_financial_discount         : new FormControl(),    // 7.58,
            type                                    : new FormControl(''),    // "M",
            
        });
    }
    
    sentInvoice(){
        console.log(JSON.stringify(this.invoiceForm.value.invoice_products));
        let products = JSON.stringify(this.invoiceForm.value.invoice_products);
        products = products.replace(/("ean")/g, 'ean');
        products = products.replace(/("invoice_quantity")/g, 'invoice_quantity');
        products = products.replace(/("percent_discount")/g, 'percent_discount');
        products = products.replace(/("unit_discount_price")/g, 'unit_discount_price');
        products = products.replace(/("unit_net_price")/g, 'unit_net_price');
        products = products.replace(/("base_calculation_icms")/g, 'base_calculation_icms');
        products = products.replace(/("base_calculation_icms_tax_substitution")/g, 'base_calculation_icms_tax_substitution');
        products = products.replace(/("percent_icms_aliquot")/g, 'percent_icms_aliquot');
        products = products.replace(/("percent_ipi_aliquot")/g, 'percent_ipi_aliquot');
        products = products.replace(/("icms_value")/g, 'icms_value');
        products = products.replace(/("st_value")/g, 'st_value');
        products = products.replace(/("icms_transferred_value")/g, 'icms_transferred_value');
        products = products.replace(/("cfop")/g, 'cfop');
        products = products.replace(/("tax_substitution_flag")/g, 'tax_substitution_flag');
        products = products.replace(/("positive_list_identifier")/g, 'positive_list_identifier');
        products = products.replace(/("product_deadline")/g, 'product_deadline');
        products = products.replace(/("product_classification")/g, 'product_classification');
        products = products.replace(/("dcb")/g, 'dcb');
        products = products.replace(/("transfer_value")/g, 'transfer_value');
        products = products.replace(/("product_total_value")/g, 'product_total_value');
        products = products.replace(/("product_total_value_charges")/g, 'product_total_value_charges');
        products = products.replace(/("tax_status_code")/g, 'tax_status_code');
        products = products.replace(/("percent_financial_discount")/g, 'percent_financial_discount');
        products = products.replace(/("percent_unit_financial_discount")/g, 'percent_unit_financial_discount');
        products = products.replace(/("type")/g, 'type');

        let query = this.apollo.mutate({
            mutation: gql`
            mutation createGroupedInvoice {
                createGroupedInvoice(
                    invoice_issue_date:"${this.invoiceForm.value.invoice_issue_date}",
                    date_time_processing:"${this.invoiceForm.value.date_time_processing}",
                    id: ${this.currentId},
                    invoice_number: ${this.invoiceForm.value.invoice_number},
                    base_calculation_icms_tax_substitution: ${this.invoiceForm.value.base_calculation_icms_tax_substitution},
                    base_calculation_icms: ${this.invoiceForm.value.base_calculation_icms},
                    danfe: "${this.invoiceForm.value.danfe}",
                    invoice_value: ${this.invoiceForm.value.invoice_value},
                    invoice_discount_value: ${this.invoiceForm.value.invoice_discount_value},
                    icms_value_transferred: ${this.invoiceForm.value.icms_value_transferred},
                    products_total_value: ${this.invoiceForm.value.products_total_value},
                    invoice_products: ${products}
                    icms_total_value_withheld: ${this.invoiceForm.value.icms_total_value_withheld},
                    icms_total_value: ${this.invoiceForm.value.icms_total_value},
                    volume_quantity: ${this.invoiceForm.value.volume_quantity},
                ) {
                    grouped_order_code
                    client_identification
                    wholesaler
                    client_code
                    commercial_condition
                    status
                    total_products
                    
                    
                }
            }
            
            `
        })
        .subscribe(({ data }) => {
            console.log(data);
            if (data.createGroupedInvoice){
                this.showSuccess('Nota encaminhada com sucesso.');
            } else{
                this.showError('Oops, houve algum problema.');
            }
        });
    }
    
    loadresponse(order){
        this.responseOrder.patchValue([ this.responseOrder ]);
    }
    
    get invoice_products(): FormArray {
        return this.invoiceForm.get('invoice_products') as FormArray;
    }
    
    public showError(error: string = '') {
        this.snackBar.open(error, '', {
            duration: 5000,
            panelClass: 'snackbar-error',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
    
    public showSuccess(message: string = '') {
        this.snackBar.open(message, '', {
            duration: 5000,
            panelClass: 'snackbar-success',
            horizontalPosition: 'left',
            verticalPosition: 'bottom'
        });
    }
}
