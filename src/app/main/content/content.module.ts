import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProhospSharedModule } from '@prohosp/shared.module';

import { ProhospContentComponent } from 'app/main/content/content.component';

@NgModule({
    declarations: [
        ProhospContentComponent
    ],
    imports     : [
        RouterModule,

        ProhospSharedModule,
    ],
    exports: [
        ProhospContentComponent
    ]
})
export class ProhospContentModule
{
}
