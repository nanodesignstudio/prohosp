export const locale = {
    lang: 'br',
    data: {
        'LOGIN': {
            'USERNAME': 'E-mail de acesso',
            'PASSWORD': 'Senha',
            'REMEMBER-ME': 'Lembrar de mim',
            'FORGOT-PASSWORD': 'Esqueceu a senha?',
            'LOGIN': 'Entrar',
            'IS-REQUIRED': 'é obrigatório.',
            'LOST-YOUR-PASSWORD': 'Perdeu sua senha?',
            'BACK-TO-LOGIN': 'Voltar para o login!',
            'SENT': 'Enviar',
        }
    }
};
