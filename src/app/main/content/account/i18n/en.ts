export const locale = {
    lang: 'en',
    data: {
        'LOGIN': {
            'USERNAME': 'Username or email',
            'PASSWORD': 'Password',
            'REMEMBER-ME': 'Remember-me',
            'FORGOT-PASSWORD': 'Forgot password?',
            'LOGIN': 'Login',
            'IS-REQUIRED': 'is required.',
            'LOST-YOUR-PASSWORD': 'Lost your password?',
            'BACK-TO-LOGIN': 'Back to login!',
            'SENT': 'Enviar',
        }
    }
};
