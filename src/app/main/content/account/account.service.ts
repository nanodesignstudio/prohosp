import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ApiConfigs } from '@prohosp/configurations/api.config'
import { headersToString } from 'selenium-webdriver/http';
import { ContentType } from '@angular/http/src/enums';

import { Environment } from '../../../../environments/environment';
import { UserEntity, UpdatePasswordEntity } from './account.interface';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';

@Injectable()
@NgModule()
export class AccountService
{
    private events: any;
    private apiUrl: string;

    constructor(private http: HttpClient, private router: Router)
    {        
        this.apiUrl = Environment.development ?
            ApiConfigs.apiSandboxUrl :
            ApiConfigs.apiUrl;
    }

    apiAuth(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl + 'api/auth/login', data, {
                headers: new HttpHeaders({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    apiLogout() {
        localStorage.removeItem('ll99webinar_user');
        this.router.navigate(['/account/login']);        
    }

    createNewPassword(guid: string, data: UpdatePasswordEntity) {
        data.guid = guid;
        return new Promise((resolve, reject) => {
            this.http.patch(`${this.apiUrl}api/auth/password`, data, {
                headers: new HttpHeaders({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }    

    getAccountSettings() {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.apiUrl}api/account/settings`, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + this.getApiToken()
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    getApiToken(): string {
        let user: UserEntity = null;
        let userData: string = localStorage.getItem('ll99webinar_user');
        if (!!userData) {
            user = JSON.parse(localStorage.getItem('ll99webinar_user'));
            if (!!user)
                return user.token;
        }
        return null;
    }

    getInitials(name: string = '', delimeter: string = ' ') {
        if (!name || name === ''){
            if (!localStorage.getItem('ll99webinar_user'))
                name = 'PR';
            else {
                const userData = JSON.parse(localStorage.getItem('ll99webinar_user'));
                name = userData.name;
            }
            
        }
        const array = name.split(delimeter);
        switch (array.length) {
            case 1:
                return array[0].charAt(0).toUpperCase() + array[0].charAt(1).toUpperCase();
            default:
                return array[0].charAt(0).toUpperCase() + array[ array.length -1 ].charAt(0).toUpperCase();
        }
    }

    getUser(): UserEntity {
        let user: UserEntity = null;
        let userData: string = localStorage.getItem('ll99webinar_user');
        if (!!userData) {
            user = JSON.parse(localStorage.getItem('ll99webinar_user'));
            if (!!user){
                return user;
            }
        }
        return null;
    }
    
    getUserInfo(){
        return new Promise((resolve, reject) => {
            this.http.get(`${this.apiUrl}api/user`, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.getApiToken()
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    isLoggedIn() {
        return new Promise<boolean>((resolve) => {
            if (!localStorage.getItem('ll99webinar_user')) {
                this.apiLogout();
                resolve(false);
            }
            else {
                this.http.get(this.apiUrl + 'api/auth/ping', {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.getApiToken()
                    })
                }).subscribe(
                    () => resolve(true), 
                    () => {
                        this.apiLogout();
                        resolve(false);
                    }
                );
            }            
        });
    }    

    requestNewPassword(data) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.apiUrl}api/auth/password`, data, {
                headers: new HttpHeaders({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    updateAccountSettings(data){
        return new Promise((resolve, reject) => {
            this.http.put(`${this.apiUrl}api/account/settings`, data, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.getApiToken()
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    updateUserInfo(data){
        return new Promise((resolve, reject) => {
            this.http.patch(`${this.apiUrl}api/user/name`, data, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.getApiToken()
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    updateUserPassword(data){
        return new Promise((resolve, reject) => {
            this.http.patch(`${this.apiUrl}api/user/password`, data, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.getApiToken()
                })
            }).subscribe((response) => {
                resolve(response);
            }, reject);
        });
    }

    updateLocalStorageInfos( key: string , value: string, storageKey: string = 'll99webinar_user'){
        const json = JSON.parse(localStorage.getItem(storageKey));
        json[key] = value;
        localStorage.setItem(storageKey, JSON.stringify(json));
    }

}
