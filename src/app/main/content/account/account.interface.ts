export interface UserEntity {
    id: string;
    name: string;
    email: string;
    photo: string;
    slug: string;
    token: string;
    expires_in: number;
}

export interface UpdatePasswordEntity {
    guid: string;
    newPassword: string;
    confirmNewPassword: string;
}

export interface AccountSetttingsEntity {
    slug: string;
    facebookId: string;
    googleAnalytics: string;
    facebookPixel: string;
    logo: string;
    logo99: boolean;
}