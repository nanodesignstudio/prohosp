import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector     : 'webinar-notifications',
    templateUrl  : './notifications.component.html',
    styleUrls    : ['./notifications.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProhospNotificationsComponent implements OnInit
{

    events = [];

    constructor()
    {
    }

    ngOnInit()
    {
        this.events = [
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'},
            {title: 'Teste de notificação', detail: 'Teste de explicação de notificações'}
        ]

    }

}
