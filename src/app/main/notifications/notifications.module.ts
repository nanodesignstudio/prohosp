import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatToolbarModule, MatDividerModule, MatListModule, MatSlideToggleModule, MatIconModule } from '@angular/material';

import { ProhospSharedModule } from '@prohosp/shared.module';

import { ProhospNotificationsComponent } from 'app/main/notifications/notifications.component';

@NgModule({
    declarations: [
        ProhospNotificationsComponent
    ],
    imports     : [
        RouterModule,

        MatToolbarModule,
        MatDividerModule,
        MatListModule,
        MatSlideToggleModule,
        MatIconModule,

        ProhospSharedModule,
    ],
    exports: [
        ProhospNotificationsComponent
    ]
})
export class ProhospNotificationsModule
{
}
