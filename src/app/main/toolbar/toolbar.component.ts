import { Component, Input } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { ProhospConfigService } from '@prohosp/services/config.service';
import { ProhospSidebarService } from '@prohosp/components/sidebar/sidebar.service';

import { AccountService } from './../content/account/account.service'


@Component({
    selector   : 'webinar-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class ProhospToolbarComponent
{
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    horizontalNav: boolean;
    noNav: boolean;

    onConfigChanged: Subscription;
    config: any;
    avatarInitials: any;

    constructor(
        private router: Router,
        private webinarConfig: ProhospConfigService,
        private sidebarService: ProhospSidebarService,
        private translate: TranslateService,
        public dialog: MatDialog,
        private accountService: AccountService
    )
    {


        this.languages = [
            {
                'id'   : 'br',
                'title': 'Português',
                'flag' : 'br'
            },
            {
                'id'   : 'en',
                'title': 'English',
                'flag' : 'us'
            },
            {
                'id'   : 'es',
                'title': 'Español',
                'flag' : 'es'
            }
        ];

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showLoadingBar = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showLoadingBar = false;
                }
            });

        this.getInitialsAvatar();

    }

    changeTheme(){
        console.log(this.webinarConfig.config.currentTheme);
        if (this.webinarConfig.config.currentTheme === 'default-theme'){
            this.webinarConfig.config.currentTheme = 'dark-theme';
        }else{
            this.webinarConfig.config.currentTheme = 'default-theme';
        }
        this.webinarConfig.setConfig(this.webinarConfig.config);
    }

    getInitialsAvatar() {
        this.avatarInitials = this.accountService.getInitials();
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    search(value)
    {
        // Do your search here...
        console.log(value);
    }

    setLanguage(lang)
    {
        // Set the selected language for toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this.translate.use(lang.id);
    }
    
    logout(){
        this.accountService.apiLogout();
    }
}
