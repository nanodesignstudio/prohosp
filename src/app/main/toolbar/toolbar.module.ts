import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatIconModule, MatMenuModule, MatProgressBarModule, MatToolbarModule, MatDialogModule } from '@angular/material';

import { ProhospSharedModule } from '@prohosp/shared.module';

import { ProhospToolbarComponent } from 'app/main/toolbar/toolbar.component';
// import { ProhospSearchBarModule } from '@prohosp/components';

// import { LiveModule } from './../content/live/live.module';


@NgModule({
    declarations: [
        ProhospToolbarComponent
    ],
    imports     : [
        RouterModule,

        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatProgressBarModule,
        MatToolbarModule,
        MatDialogModule,

        ProhospSharedModule,
        // ProhospSearchBarModule,
        // RoomModule,
        // LiveModule
    ],
    exports     : [
        ProhospToolbarComponent
    ]
})
export class ProhospToolbarModule
{
}
