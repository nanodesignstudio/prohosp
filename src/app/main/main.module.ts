import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatSidenavModule } from '@angular/material';

import { ProhospSharedModule } from '@prohosp/shared.module';
import { ProhospNavigationModule, ProhospSidebarModule, } from '@prohosp/components';

import { ProhospContentModule } from 'app/main/content/content.module';
import { ProhospFooterModule } from 'app/main/footer/footer.module';
import { ProhospNavbarModule } from 'app/main/navbar/navbar.module';
import { ProhospNotificationsModule } from 'app/main/notifications/notifications.module';
import { ProhospToolbarModule } from 'app/main/toolbar/toolbar.module';

import { ProhospMainComponent } from './main.component';


@NgModule({
    declarations: [
        ProhospMainComponent,
    ],
    imports     : [
        RouterModule,

        MatSidenavModule,

        ProhospSharedModule,

        // ProhospThemeOptionsModule,
        ProhospNavigationModule,
        // ProhospSearchBarModule,
        ProhospSidebarModule,

        ProhospContentModule,
        ProhospFooterModule,
        ProhospNavbarModule,
        ProhospNotificationsModule,
        ProhospToolbarModule,
    ],
    exports     : [
        ProhospMainComponent
    ]
})
export class ProhospMainModule
{
}
