import { Component, Input, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { ProhospPerfectScrollbarDirective } from '@prohosp/directives/webinar-perfect-scrollbar/webinar-perfect-scrollbar.directive';
import { ProhospSidebarService } from '@prohosp/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import { ProhospNavigationService } from '@prohosp/components/navigation/navigation.service';

@Component({
    selector     : 'webinar-navbar',
    templateUrl  : './navbar.component.html',
    styleUrls    : ['./navbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProhospNavbarComponent implements OnDestroy
{
    private webinarPerfectScrollbar: ProhospPerfectScrollbarDirective;

    @ViewChild(ProhospPerfectScrollbarDirective) set directive(theDirective: ProhospPerfectScrollbarDirective)
    {
        if ( !theDirective )
        {
            return;
        }

        this.webinarPerfectScrollbar = theDirective;

        this.navigationServiceWatcher =
            this.navigationService.onItemCollapseToggled.subscribe(() => {
                this.webinarPerfectScrollbarUpdateTimeout = setTimeout(() => {
                    this.webinarPerfectScrollbar.update();
                }, 310);
            });
    }

    @Input() layout;
    navigation: any;
    navigationServiceWatcher: Subscription;
    webinarPerfectScrollbarUpdateTimeout;

    constructor(
        private sidebarService: ProhospSidebarService,
        private navigationService: ProhospNavigationService
    )
    {
        // Navigation data
        this.navigation = navigation;

        // Default layout
        this.layout = 'vertical';
    }

    ngOnDestroy()
    {
        if ( this.webinarPerfectScrollbarUpdateTimeout )
        {
            clearTimeout(this.webinarPerfectScrollbarUpdateTimeout);
        }

        if ( this.navigationServiceWatcher )
        {
            this.navigationServiceWatcher.unsubscribe();
        }
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    toggleSidebarFolded(key)
    {
        this.sidebarService.getSidebar(key).toggleFold();
    }
}
