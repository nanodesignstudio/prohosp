import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatIconModule } from '@angular/material';

import { ProhospSharedModule } from '@prohosp/shared.module';

import { ProhospNavbarComponent } from 'app/main/navbar/navbar.component';
import { ProhospNavigationModule } from '@prohosp/components';

@NgModule({
    declarations: [
        ProhospNavbarComponent
    ],
    imports     : [
        RouterModule,

        MatButtonModule,
        MatIconModule,

        ProhospSharedModule,
        ProhospNavigationModule
    ],
    exports     : [
        ProhospNavbarComponent
    ]
})
export class ProhospNavbarModule
{
}
