import { Injectable, NgModule } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AccountService } from './main/content/account/account.service';

@Injectable()
@NgModule({
	imports: [AccountService]
})
export class AuthGuard implements CanActivate {
  	constructor (
		  private user: AccountService,
		  private router: Router) {}

	canActivate(): Promise<boolean> {
		return this.user.isLoggedIn();
	}
}