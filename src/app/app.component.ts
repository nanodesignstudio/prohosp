import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiConfigs } from '@prohosp/configurations/api.config';

import { HttpHeaders } from '@angular/common/http';
import { Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';


import { ProhospSplashScreenService } from '@prohosp/services/splash-screen.service';
import { ProhospTranslationLoaderService } from '@prohosp/services/translation-loader.service';
import { ProhospNavigationService } from '@prohosp/components/navigation/navigation.service';

import { locale as navigationPortuguese } from './navigation/i18n/br';
import { locale as navigationEnglish } from './navigation/i18n/en';
import { locale as navigationEspanol } from './navigation/i18n/es';
import { longStackSupport } from 'q';
import gql from 'graphql-tag';

@Component({
  selector   : 'webinar-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.scss']
})
export class AppComponent
{
  constructor(
    private translate: TranslateService,
    private webinarNavigationService: ProhospNavigationService,
    private webinarSplashScreen: ProhospSplashScreenService,
    private webinarTranslationLoader: ProhospTranslationLoaderService,   
    httpLink: HttpLink,
    apollo: Apollo,
    )
    {
      // Add languages
      this.translate.addLangs(['br', 'en', 'tr']);
      
      // Set the default language
      this.translate.setDefaultLang('br');
      
      // Set the navigation translations
      this.webinarTranslationLoader.loadTranslations(navigationPortuguese, navigationEnglish, navigationEspanol);
      
      // Use a language
      this.translate.use('br');
      
      // const http = httpLink.create({uri: 'https://homologa.fidelize.com.br/graphql'});
       const http = httpLink.create({uri: 'https://gateway.fidelize.com.br/graphql'});
      
      
      if(localStorage.getItem('refreshToken') === '1' || !!!localStorage.getItem('refreshToken') || !!!localStorage.getItem('token')){
        apollo.create({
          link: http,
          cache: new InMemoryCache()
        });
        
        
        let query = apollo.mutate<any>({
          mutation: gql`
          mutation createToken {
            createToken(login: "prohosp", password: "052LtZMV") {
              token
            }
          }
          `
        })
        .subscribe(({ data }) => {
          localStorage.setItem('refreshToken', '0' );
          localStorage.setItem('token', data.createToken.token);
          window.location.reload();
        });
      } else{ 
        const logoutLink = onError(( a ) => {
          if (a.networkError['status'] === 500) {
            console.log('ERRO: ', a.networkError['status']);
            localStorage.setItem('refreshToken', '1' );
            window.location.reload();
          };
        })
        
        const auth = setContext((_, {  }) => {
          const headers = new HttpHeaders(); 
          const token = localStorage.getItem('token');
          if (!token) {
            return {};
          } else {
            return {
              headers: headers.append('Authorization', `Bearer ${token}`).append('Content-Type','application/json')
            };
          }
        }).concat(logoutLink);
        apollo.create({
          link: auth.concat(http),
          cache: new InMemoryCache()
        });
      }
    }
  }
  