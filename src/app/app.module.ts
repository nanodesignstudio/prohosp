// Angular core
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ApolloModule } from 'apollo-angular';
import { HttpLinkModule } from 'apollo-angular-link-http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
// Angular Material Core
import { MaterialModule } from './material.shared';
import 'hammerjs';
import { ProhospWidgetModule } from '@prohosp/components/widget/widget.module';
import { 
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material';
// Customized Angular Material Components
import { Routing, appRoutingProviders } from './app-routing.module';
import { ProhospMaterialColorPickerModule } from '@prohosp/components/material-color-picker/material-color-picker.module';
import { ProhospConfirmDialogModule } from '@prohosp/components';
// External packages and webinar core itens
import { ProhospModule } from '@prohosp/webinar.module';
import { ProhospSharedModule } from '@prohosp/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { webinarConfig } from './webinar-config';
// Internal components
// Includes components
import { AppComponent } from './app.component';

// // ********** Account *********
// import { ProhospLoginComponent } from './main/content/account/login/login.component';
// import { PasswordResetComponent } from './main/content/account/password_reset/password_reset.component';
// import { UpdatePasswordComponent } from './main/content/account/password_reset/update/update_password.component';
// import { ProfileSettingsComponent } from './main/content/account/settings/profile/profile.component';
// import { AccountSettingsComponent } from './main/content/account/settings/settings/settings.component';

// // ********** Dashboard *********
// import { DashboardComponent } from './main/content/dashboard/dashboard.component';
// // ********** Live *********
// import { LiveComponent } from './main/content/live/live.component';
// import { SubscribeComponent } from './main/content/live/subscribe/subscribe.component';
// // ********** Room *********
// import { ProhospRoomEditComponent } from './main/content/room/edit/edit.component';
// import { ConfigureProhospDialogComponent } from './main/content/room/includes/configure/configure.component';

// // Includes Modules
 import { ProhospMainModule } from './main/main.module';
// // Services
// import { AuthGuard } from './auth.guard';
// // Facebook Module
// import { FacebookModule } from 'ngx-facebook';

// Prohosp
import { OrdersComponent } from './main/content/orders/orders.component'
import { EditComponent } from './main/content/orders/edit/edit.component'
import { CreateComponent } from './main/content/orders/create/create.component'
import { ListComponent } from './main/content/orders/list/list.component'
import { ResponseComponent } from './main/content/orders/response/response.component'
import { InvoiceComponent } from './main/content/orders/invoice/invoice.component'
import { OrdersService } from './main/content/orders/orders.service'

 
@NgModule({
    declarations: [
        AppComponent,

        // Prohosp
        OrdersComponent,
        EditComponent,
        CreateComponent,
        ListComponent,
        ResponseComponent,
        InvoiceComponent
    ],
    imports     : [
        // Angular Core
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ApolloModule,
        HttpLinkModule,
        Routing,
        TranslateModule.forRoot(),
        // FacebookModule.forRoot(),        
        MaterialModule,
        ProhospMaterialColorPickerModule,
        
        // Prohosp Main and Shared modules
        ProhospModule.forRoot(webinarConfig),
        ProhospWidgetModule,
        ProhospSharedModule,
        ProhospMainModule,
        // AuthGuard,
        ProhospConfirmDialogModule        
    ],
    providers: [
        appRoutingProviders,
        ApolloModule,
        OrdersService
    ],
    entryComponents: [
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
