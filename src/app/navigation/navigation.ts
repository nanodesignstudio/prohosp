export const navigation = [
    {
        'id'      : 'menu',
        'title'   : 'Menu',
        'translate': 'NAV.MENU',
        'type'    : 'group',
        'children': [
            {
                'id'   : 'home',
                'title': 'Home',
                'translate': 'NAV.HOME.TITLE',
                'type' : 'item',
                'icon' : 'home',
                'url'  : '/dashboard'
            },
            // {
            //     'id'   : 'create',
            //     'title': 'Create webinar',
            //     'translate': 'NAV.CREATE.TITLE',
            //     'type' : 'item',
            //     'icon' : 'add',
            //     'url'  : '/room/create'
            // },
            // {
            //     'id'   : 'myaccount',
            //     'title': 'My Account',
            //     'translate': 'NAV.MYACCOUNT.TITLE',
            //     'type' : 'item',
            //     'icon' : 'face',
            //     'url'  : '/account/login'
            // },
            // {
            //     'id'   : 'myAccount',
            //     'title': 'My Account',
            //     'translate': 'NAV.MYACCOUNT.TITLE',
            //     'type' : 'item',
            //     'icon' : 'add',
            //     'url'  : '/account/password_reset'
            // }
        ]
    },
    {
        'id'      : 'settings',
        'title'   : 'Ferramentas',
        'translate': 'NAV.MENU.TOOLS',
        'type'    : 'group',
        'children': [
            {
                'id'   : 'profile',
                'title': 'Perfil',
                'translate': 'NAV.PROFILE.TITLE',
                'type' : 'item',
                'icon' : 'add',
                'url'  : '/account/profile'
            },
            {
                'id'   : 'settings',
                'title': 'Configurações',
                'translate': 'NAV.SETTINGS.TITLE',
                'type' : 'item',
                'icon' : 'add',
                'url'  : '/account/settings'
            },
            {
                'id'   : 'logout',
                'title': 'Logout',
                'translate': 'NAV.LOGOUT.TITLE',
                'type' : 'item',
                'icon' : 'add',
                'url'  : '/account/password_reset'
            },
        ]
    },
    // {
    //     'id'      : 'user-interface',
    //     'title'   : 'User Interface',
    //     'type'    : 'group',
    //     'icon'    : 'web',
    //     'children': [
    //         {
    //             'id'   : 'forms',
    //             'title': 'Forms',
    //             'type' : 'item',
    //             'icon' : 'web_asset',
    //             'url'  : '/documentation/forms'
    //         },
    //         {
    //             'id'   : 'icons',
    //             'title': 'Icons',
    //             'type' : 'item',
    //             'icon' : 'photo',
    //             'url'  : '/documentation/icons'
    //         },
    //         {
    //             'id'   : 'typography',
    //             'title': 'Typography',
    //             'type' : 'item',
    //             'icon' : 'text_fields',
    //             'url'  : '/documentation/typography'
    //         },
    //         {
    //             'id'   : 'helper-classes',
    //             'title': 'Helper Classes',
    //             'type' : 'item',
    //             'icon' : 'help',
    //             'url'  : '/documentation/helper-classes'
    //         },
    //         {
    //             'id'      : 'page-layouts',
    //             'title'   : 'Page Layouts',
    //             'type'    : 'collapse',
    //             'icon'    : 'view_quilt',
    //             'children': [
    //                 {
    //                     'id'      : 'carded',
    //                     'title'   : 'Carded',
    //                     'type'    : 'collapse',
    //                     'badge'   : {
    //                         'title': 10,
    //                         'bg'   : '#525e8a',
    //                         'fg'   : '#FFFFFF'
    //                     },
    //                     'children': [
    //                         {
    //                             'id'   : 'full-width',
    //                             'title': 'Full Width',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/full-width'
    //                         },
    //                         {
    //                             'id'   : 'full-width-2',
    //                             'title': 'Full Width 2',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/full-width-2'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav',
    //                             'title': 'Left Sidenav',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/left-sidenav'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav-tabbed',
    //                             'title': 'Left Sidenav Tabbed',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/left-sidenav-tabbed'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav-2',
    //                             'title': 'Left Sidenav 2',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/left-sidenav-2'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav-2-tabbed',
    //                             'title': 'Left Sidenav 2 Tabbed',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/left-sidenav-2-tabbed'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav',
    //                             'title': 'Right Sidenav',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/right-sidenav'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav-tabbed',
    //                             'title': 'Right Sidenav Tabbed',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/right-sidenav-tabbed'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav-2',
    //                             'title': 'Right Sidenav 2',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/right-sidenav-2'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav-2-tabbed',
    //                             'title': 'Right Sidenav 2 Tabbed',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/carded/right-sidenav-2-tabbed'
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     'id'      : 'simple',
    //                     'title'   : 'Simple',
    //                     'type'    : 'collapse',
    //                     'badge'   : {
    //                         'title': 8,
    //                         'bg'   : '#525e8a',
    //                         'fg'   : '#FFFFFF'
    //                     },
    //                     'children': [
    //                         {
    //                             'id'   : 'full-width',
    //                             'title': 'Full Width',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/full-width'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav',
    //                             'title': 'Left Sidenav',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/left-sidenav'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav-2',
    //                             'title': 'Left Sidenav 2',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/left-sidenav-2'
    //                         },
    //                         {
    //                             'id'   : 'left-sidenav-3',
    //                             'title': 'Left Sidenav 3',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/left-sidenav-3'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav',
    //                             'title': 'Right Sidenav',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/right-sidenav'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav-2',
    //                             'title': 'Right Sidenav 2',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/right-sidenav-2'
    //                         },
    //                         {
    //                             'id'   : 'right-sidenav-3',
    //                             'title': 'Right Sidenav 3',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/right-sidenav-3'
    //                         },
    //                         {
    //                             'id'   : 'tabbed',
    //                             'title': 'Tabbed',
    //                             'type' : 'item',
    //                             'url'  : '/documentation/page-layouts/simple/tabbed'
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     'id'   : 'blank',
    //                     'title': 'Blank',
    //                     'type' : 'item',
    //                     'url'  : '/documentation/page-layouts/blank'
    //                 }
    //             ]
    //         },
    //         {
    //             'id'   : 'colors',
    //             'title': 'Colors',
    //             'type' : 'item',
    //             'icon' : 'color_lens',
    //             'url'  : '/documentation/colors'
    //         }
    //     ]
    // },
];
