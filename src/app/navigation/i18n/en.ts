export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'MENU': 'Menu',
            'HOME'        : {
                'TITLE': 'Home'
            },
            'CREATE'        : {
                'TITLE': 'Create Prohosp'
            },
            'MYACCOUNT'        : {
                'TITLE': 'My Account'
            },
            'SUPPORT'        : {
                'TITLE': 'Support'
            }
        }
    }
};
