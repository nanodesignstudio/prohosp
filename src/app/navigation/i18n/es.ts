export const locale = {
    lang: 'es',
    data: {
        'NAV': {
            'MENU': 'Menu',
            'HOME'        : {
                'TITLE': 'Início'
            },
            'CREATE'        : {
                'TITLE': 'Criar Prohosp'
            },
            'MYACCOUNT'        : {
                'TITLE': 'Minha conta'
            },
            'SUPPORT'        : {
                'TITLE': 'Suporte'
            }
        }
    }
};
